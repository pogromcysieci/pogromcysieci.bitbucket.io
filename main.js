'use strict';
function getNumericInput(name, matrixItemNumberValue) {
    let input = document.createElement('input');
    input.setAttribute("type", "number");
    input.setAttribute("name", name);
    input.setAttribute("id", name);
    input.setAttribute("value", matrixItemNumberValue);
    //input.setAttribute("required", true);
    return input;
}

function setResult(header, result, id) {
    let output = document.getElementById(id);
    let text = "<h2>" + header + "</h2>";
    for(let i = 0; i < result.length; i++) {
        text += "<p>" + result[i] + ", </p>";
    }
    output.innerHTML = text;
}

function clearContainer(id) {
    let item = document.getElementById(id);
    while(item.firstChild){
        item.removeChild(item.firstChild);
    }
}

function generateMatrix() {
    let rows = document.getElementById('rows').value;
    let columns = document.getElementById('columns').value;
    let matrix = document.getElementById("matrix");
    let matrixItemNumberValue = 1;
    
    let div = document.createElement('div');
    div.setAttribute('class', 'matrix--container');
    div.style.gridTemplateColumns = `repeat(${columns}, minmax(20px, auto))`;
    div.style.gridTemplateRows = `repeat(${rows}, minmax(20px, auto))`;

    clearContainer('message');
    clearContainer('matrix');

    for (let i = 1; i <= rows; i++) {
        for (let j = 1; j <= columns; j++) {
            let name = `number_${i}_${j}`;
            let span = document.createElement('span');
            span.setAttribute('id', `span_${i}_${j}`);
            span.append(getNumericInput(name, matrixItemNumberValue));
            div.append(span);
            matrixItemNumberValue += 1;
        }
    }
    matrix.append(div);
}

function getMatrixValues() {
    let rows = document.getElementById('rows').value;
    let columns = document.getElementById('columns').value;
    let matrix = [];

    for (let i = 1; i <= rows; i++) {
        let row = [];
        for (let j = 1; j <= columns; j++) {
            let name = `number_${i}_${j}`;
            let value = document.getElementById(name).value;
            row.push(value);
        }
        matrix.push(row);
    }
    return matrix;
}

function validateForm() {
    let warrnings = [];
    let form = document.forms["theForm"];
    for(let i = 0; i < form.length - 1; i++) {
        if(form[i].value.length == 0) warrnings.push(form[i].id + ' is empty');
        if(isNaN(form[i].value)) warrnings.push(form[i].id + ' is not a number');
        if((form[i].id == 'rows' || form[i].id == 'columns') &&  form[i].value != "" && form[i].value <= 0) warrnings.push(form[i].id + ' is less or equal than 0');
    }

    if(warrnings && warrnings.length) {
        setResult('Warnings', warrnings, 'warnings');
        clearContainer('message');
        return false;
    } else {
        clearContainer('warnings');
        return true;
    }
}

function spiralMatrixProblem() {
    let matrix = getMatrixValues();
    let result = [];
    let startingRowIndex = 0;
    let endingRowIndex = matrix.length;
    let startingColumnIndex = 0;
    let endingColumnIndex = (matrix && matrix[0] && matrix[0].length) ? matrix[0].length : 0;

    if(validateForm() === false) {
        return false;
    }

    while (startingRowIndex < endingRowIndex && startingColumnIndex < endingColumnIndex) {
        //get row to right
        for(let i = startingColumnIndex; i < endingColumnIndex; i++) {
            result.push(matrix[startingRowIndex][i]);
            document.getElementById(`span_${startingRowIndex+1}_${i+1}`).classList.add("matrix--span__right");
        }
        startingRowIndex += 1;

        //get column to bottom
        for(let j = startingRowIndex; j < endingRowIndex; j++) {
            result.push(matrix[j][endingColumnIndex - 1]);
            document.getElementById(`span_${j+1}_${endingColumnIndex}`).classList.add("matrix--span__bottom");
        }
        endingColumnIndex -= 1;

        //get row to left
        if(startingRowIndex < endingRowIndex) {
            for(let k = endingColumnIndex - 1; k > startingColumnIndex - 1; k--) {
                result.push(matrix[endingRowIndex - 1][k]);
                document.getElementById(`span_${endingRowIndex}_${k+1}`).classList.add("matrix--span__left");
            }
            endingRowIndex -= 1;
        }

        //get column to top
        if(startingColumnIndex < endingColumnIndex) {
            for(let l = endingRowIndex - 1; l > startingRowIndex - 1; l--) {
                result.push(matrix[l][startingColumnIndex]);
                document.getElementById(`span_${l+1}_${startingColumnIndex+1}`).classList.add("matrix--span__top");
            }
            startingColumnIndex += 1;
        }

    }
    setResult('Result:', result, 'message');
    return false;
}

function init() {
    document.getElementById('rows').onchange = generateMatrix;
    document.getElementById('columns').onchange = generateMatrix;
    document.getElementById('theForm').onsubmit = spiralMatrixProblem;
    document.getElementById('theForm').onchange = validateForm;
}

window.onload = init;